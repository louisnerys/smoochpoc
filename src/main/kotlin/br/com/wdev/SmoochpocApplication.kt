package br.com.wdev

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.context.web.SpringBootServletInitializer

@SpringBootApplication
open class SmoochPOCApplication

class ServletInitializer : SpringBootServletInitializer() {
	override fun configure(application: SpringApplicationBuilder): SpringApplicationBuilder {
		return application.sources(SmoochPOCApplication::class.java)
	}
}

fun main(args: Array<String>) {
	SpringApplication.run(SmoochPOCApplication::class.java, *args)
}