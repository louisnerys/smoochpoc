package br.com.wdev.smoochpoc.spring

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.ldap.core.support.LdapContextSource
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.ldap.DefaultSpringSecurityContextSource

@Configuration
@EnableWebSecurity
open class WebSecurityConfig : WebSecurityConfigurerAdapter() {

	@Throws(Exception::class)
	override fun configure(http: HttpSecurity) {
		http
				.authorizeRequests().antMatchers(HttpMethod.POST, "/chat-user").permitAll()
				.and().authorizeRequests().anyRequest().fullyAuthenticated()
				.and().httpBasic()
				.and().csrf().disable()
	}

	@Bean open fun contextSource(@Value("\${ldap.url}") ldapUrl: String) = DefaultSpringSecurityContextSource(ldapUrl)

	@Configuration open class AuthenticationConfiguration : GlobalAuthenticationConfigurerAdapter() {

		@Autowired lateinit var contextSource: LdapContextSource

		@Throws(Exception::class)
		override fun init(auth: AuthenticationManagerBuilder) {

			auth.ldapAuthentication()
					.userSearchFilter("(&(uid={0}))")
					.contextSource(contextSource)
					.userDetailsContextMapper(UserMapper())
		}
	}
}