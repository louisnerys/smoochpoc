package br.com.wdev.smoochpoc.spring

import org.springframework.ldap.core.DirContextAdapter
import org.springframework.ldap.core.DirContextOperations
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.ldap.userdetails.PersonContextMapper
import org.springframework.security.ldap.userdetails.UserDetailsContextMapper

class User : UserDetails {

	private val password: String
	private val delegate: UserDetails

	val name: String
	val surname: String
	val email: String

	constructor(user: UserDetails, ctx: DirContextOperations) {
		this.delegate = user
		this.password = user.password
		this.name = ctx.getStringAttributes("cn")[0]
		this.surname = ctx.getStringAttribute("sn")
		this.email = ctx.getStringAttribute("mail")
	}

	fun printable() = "$name $surname: $email"

	override fun getPassword(): String {
		return password
	}

	override fun getAuthorities(): Collection<GrantedAuthority> {
		return delegate.authorities
	}

	override fun getUsername(): String {
		return delegate.username
	}

	override fun isAccountNonExpired(): Boolean {
		return delegate.isAccountNonExpired
	}

	override fun isAccountNonLocked(): Boolean {
		return delegate.isAccountNonLocked
	}

	override fun isCredentialsNonExpired(): Boolean {
		return delegate.isCredentialsNonExpired
	}

	override fun isEnabled(): Boolean {
		return delegate.isEnabled
	}
}

class UserMapper : UserDetailsContextMapper {

	override fun mapUserFromContext(ctx: DirContextOperations, username: String,
									authorities: Collection<GrantedAuthority>): UserDetails {
		return User(PersonContextMapper().mapUserFromContext(ctx, username, authorities), ctx)
	}

	override fun mapUserToContext(user: UserDetails, ctx: DirContextAdapter) { }
}
