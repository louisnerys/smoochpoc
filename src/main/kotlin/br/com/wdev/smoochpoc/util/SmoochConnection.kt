package br.com.wdev.smoochpoc.util

import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.commons.codec.binary.Base64
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec

@Component open class SecurityUtil {
	data class Header (val alg: String = "HS256", val typ: String = "JWT", val kid: String)
	data class Payload (val scope: String)

	private val message: String
	private val hash: String

	private val objectMapper = ObjectMapper()

	@Autowired constructor(@Value("\${smooch.security.key}") key: String, @Value("\${smooch.security.secret}") secret: String) {
		val sha256_HMAC = Mac.getInstance("HmacSHA256")
		sha256_HMAC.init(SecretKeySpec(secret.toByteArray(), "HmacSHA256"))

		message = "${getJsonBase64(Header(kid = key))}.${getJsonBase64(Payload("app"))}"
		hash = Base64.encodeBase64URLSafeString(sha256_HMAC.doFinal(message.toByteArray()))
	}

	private fun getJsonBase64(obj: Any) = Base64.encodeBase64URLSafeString(objectMapper.writeValueAsString(obj).toByteArray())

	fun getJwt() = "$message.$hash"
}

@Component open class SmoochConnection
@Autowired constructor(private val sec: SecurityUtil) {
	private val restTemplate: RestTemplate = RestTemplate()

	private val httpHeaders: HttpHeaders
		get() {
			val headers = HttpHeaders()
			headers.add("Authorization", "Bearer ${sec.getJwt()}")

			return headers
		}

	fun <T> get(url: String, clazz: Class<T>, vararg variables: Any) : T =
			restTemplate.exchange(url, HttpMethod.GET, HttpEntity("", httpHeaders), clazz, *variables).body

	fun <T> post(url: String, request: Any, clazz: Class<T>, vararg variables: Any) : T =
			restTemplate.exchange(url, HttpMethod.POST, HttpEntity(request, httpHeaders), clazz, *variables).body
}