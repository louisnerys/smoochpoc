package br.com.wdev.smoochpoc.service

import br.com.wdev.smoochpoc.util.SmoochConnection
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

interface SmoochService {
	fun update(chatId: String): Any
	fun send(chatId: String, payload: Any): Any
	fun list(): Any
}

@Service open class SmoochServiceImpl
@Autowired constructor(private val connection: SmoochConnection) : SmoochService {
	private val base = "https://api.smooch.io/v1/appusers"
	private val conversation = "$base/{chatId}/conversation"

	override fun update(chatId: String) : Any =
			connection.get("$conversation", Any::class.java, chatId)

	override fun send(chatId: String, payload: Any) : Any =
			connection.post("$conversation/messages", payload, Any::class.java, chatId)

	override fun list() : Any =
			connection.get(base, Any::class.java)
}