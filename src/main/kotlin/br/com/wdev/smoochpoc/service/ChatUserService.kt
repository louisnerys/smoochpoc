package br.com.wdev.smoochpoc.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Service

class ChatUser {
	@Id lateinit var _id: String
	var userId: String? = null
	var name: String? = null
}

interface ChatUserRepository : MongoRepository<ChatUser, String> { }

interface ChatUserService {
	fun list(): List<ChatUser>
	fun save(chatUser: ChatUser): ChatUser
}

@Service open class ChatUserServiceImpl
@Autowired constructor(val chatUserRepository: ChatUserRepository): ChatUserService {
	override fun list(): List<ChatUser> = chatUserRepository.findAll()
	override fun save(chatUser: ChatUser): ChatUser = chatUserRepository.save(chatUser)
}