package br.com.wdev.smoochpoc.controller

import br.com.wdev.smoochpoc.spring.User
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
open class FirstController {
	@RequestMapping("email") fun printMail(principal: UsernamePasswordAuthenticationToken) : Any {
		if (principal.principal is User) {
			return  (principal.principal as User).printable()
		}

		throw Exception()
	}
}

@Controller
open class IndexController {
	@RequestMapping("/") fun index()= "index"
}