package br.com.wdev.smoochpoc.controller

import br.com.wdev.smoochpoc.service.ChatUser
import br.com.wdev.smoochpoc.service.ChatUserService
import br.com.wdev.smoochpoc.service.SmoochService
import br.com.wdev.smoochpoc.spring.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.HttpClientErrorException

@RestController @RequestMapping("chat") open class ChatController
@Autowired constructor(val smoochService: SmoochService) {

	@RequestMapping(value="{chatId}", method = arrayOf(RequestMethod.POST))
	fun send(@RequestBody request: MessageRequest, @PathVariable chatId: String, principal: UsernamePasswordAuthenticationToken) : Any {
		if (principal.principal is User) {
			val user = principal.principal as User

			val payload = Message()
			payload.name = "${user.name} ${user.surname}"
			payload.email = user.email
			payload.text = request.message

			return smoochService.send(chatId, payload)
		}

		throw Exception("User not authorized")
	}

	@RequestMapping(value="{chatId}", method = arrayOf(RequestMethod.GET))
	fun update(@PathVariable chatId: String) = smoochService.update(chatId)

	@RequestMapping(method = arrayOf(RequestMethod.GET))
	fun list() = smoochService.list()

	@ExceptionHandler(HttpClientErrorException::class)
	fun error(ex: HttpClientErrorException) : ResponseEntity<String> = ResponseEntity(ex.responseBodyAsString, ex.statusCode)
}

@RestController @RequestMapping("chat-user") open class ChatUsersController
@Autowired constructor(val service: ChatUserService) {

	@RequestMapping(method = arrayOf(RequestMethod.GET))
	fun list() = service.list()

	@RequestMapping(method = arrayOf(RequestMethod.POST))
	fun newMessage(@RequestBody request: NewMessage): ChatUser {
		try {
			request.appUser.name = request.messages[0].name
		} catch (ignored: Exception) { }

		return service.save(request.appUser)
	}

	@ExceptionHandler(HttpClientErrorException::class)
	fun error(ex: HttpClientErrorException) : ResponseEntity<String> = ResponseEntity(ex.responseBodyAsString, ex.statusCode)
}

class Message {
	val role: String = "appMaker"
	var name: String? = null
	var email: String? = null
	lateinit var text: String
}

class MessageRequest {
	lateinit var message: String
}

class NewMessage {
	lateinit var messages: List<Message>
	lateinit var appUser: ChatUser
}