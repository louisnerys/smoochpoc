angular.module('SmoochPOC',['ngMaterial', 'ngMessages', 'luegg.directives'])

    .config(['$mdThemingProvider', function($mdThemingProvider){
        $mdThemingProvider.theme('default')
            .primaryPalette('light-green');
    }])

    .controller('smoochCtrl', function($scope, $http, $interval) {
        $scope.glued = true;
        $scope.message = "";
        $scope.messages = [];
        $scope.users = [];

        var currentChat = "";

        var reloadConversations = function () {
            $http.get("/chat-user")
                .then(
                    function success(response) { $scope.users = response.data; },
                    function failure(response) { console.log("failure", response) });

            if (!!currentChat) {
                $http.get("/chat/" + currentChat)
                    .then(
                        function success(response) {
                            $scope.messages = response.data.conversation.messages;
                        },
                        function failure(response) {
                            console.log("failure", response)
                        });
            }
        };

        $scope.sendMessage = function () {
            $http.post("/chat/" + currentChat, { "message": $scope.message })
                .then(
                    function success(response) { reloadConversations(response) },
                    function failure(response) { console.log("failure", response) });
            $scope.message = "";
        };

        $scope.changeChat = function(chatId) {
            currentChat = chatId;
            reloadConversations();
        };

        $scope.showIcon = function(chatId) {
            return currentChat === chatId;
        };

        reloadConversations();

        $interval(reloadConversations, 5000);

        window.$scope = $scope;

    });